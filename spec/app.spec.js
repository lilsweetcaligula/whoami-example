const App = require('../src/app.js')

describe('App', () => {
  describe('.initialize', () => {
    it('initializes the application', () => {
      const app = App.initialize()
      expect(app).toEqual(jasmine.any(Function))
    })
  })
})

