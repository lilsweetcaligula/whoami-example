const supertest = require('supertest')
const App = require('../../src/app.js')
const ExpressUtils = require('../../src/lib/express_utils.js')
const { hasProps } = require('../support/matchers.js')(jasmine)

describe('whoami resource', () => {
  describe('show', () => {
    const requestWhoAmI = (app = App.initialize()) =>
      supertest(app).get('/whoami')

    it('responds with the correct response body', async () => {
      await requestWhoAmI()
        .expect(200)
        .expect(res => {
          expect(res.body).toEqual(
            hasProps(['ip_address', 'lang', 'user_agent'])
          )
        })
    })

    it('includes the user agent', async () => {
      const fake_user_agent = 'curl/7.58.0'

      await requestWhoAmI()
        .set('user-agent', fake_user_agent)
        .expect(200)
        .expect(res => {
          expect(res.body.user_agent).toEqual(fake_user_agent)
        })
    })

    it("includes the client's language preference", async () => {
      const fake_lang = 'en-US,en;q=0.9'

      await requestWhoAmI()
        .set('accept-language', fake_lang)
        .expect(200)
        .expect(res => {
          expect(res.body.lang).toEqual(fake_lang)
        })
    })

    it("includes the client's ip address", async () => {
      const fake_ip = '::ffff:127.0.0.2'

      spyOn(ExpressUtils, 'getRequestIp').and.returnValue(fake_ip)

      await requestWhoAmI()
        .expect(200)
        .expect(res => {
          expect(res.body.ip_address).toEqual(fake_ip)
          expect(ExpressUtils.getRequestIp).toHaveBeenCalled()
        })
    })
  })
})

