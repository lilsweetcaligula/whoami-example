const Functools = {
  getOrDefault: (o, k, opts) => k in o ? o[k] : opts.default
}

module.exports = Functools
