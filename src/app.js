const Express = require('express')
const { handleAppError } = require('./middleware/handle_app_error.js')
const WhoAmI = require('./whoami/routes.js')

exports.initialize = () =>
  Express()
    .use('/whoami', WhoAmI.initialize())
    .use(handleAppError())

