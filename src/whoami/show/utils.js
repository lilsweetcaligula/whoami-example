const assert = require('assert-plus')
const { getOrDefault } = require('../../lib/functools.js')
const ExpressUtils = require('../../lib/express_utils.js')

const Utils = {
  getRequestIp: (...args) => ExpressUtils.getRequestIp(...args),

  getRequestLang: req => {
    assert.object(req, 'req')
    assert.object(req.headers, 'req.headers')

    return getOrDefault(req.headers, 'accept-language', { default: null })
  },

  getRequestUserAgent: req => {
    assert.object(req, 'req')
    assert.object(req.headers, 'req.headers')

    return getOrDefault(req.headers, 'user-agent', { default: null })
  }
}

module.exports = Utils
