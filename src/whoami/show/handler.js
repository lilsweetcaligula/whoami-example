const {
  getRequestIp,
  getRequestLang,
  getRequestUserAgent
} = require('./utils.js')

exports.initialize = () => (req, res) => res.json({
  ip_address: getRequestIp(req),
  lang: getRequestLang(req),
  user_agent: getRequestUserAgent(req)
})
