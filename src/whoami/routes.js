const Express = require('express')
const ShowWhoAmI = require('./show/handler.js')

exports.initialize = () => 
  Express.Router()
    .use('/', ShowWhoAmI.initialize())
